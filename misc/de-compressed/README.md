# de-compressed

## Description

```
As an elite cyber security expert, you've been tasked with uncovering the secrets hidden within a message intercepted from a notorious spy. We suspect there may be more to this message than meets the eye. Can you use your skills in steganography to uncover whatever else might be hiding? The fate of national security is in your hands.
```

### Flag

`dam{t1m3_t0_kick_b4ck_4nd_r3l4x}`

## Checklist for Author

* [x] Challenge compiles
* [x] Dockerfile has been updated
* [x] `base-container/run_chal.sh` has been updated
* [x] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [x] There is at least one test exploit/script in the `tests` directory
* [x] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

https://www.codeproject.com/Articles/13808/Steganography-16-Hiding-additional-files-in-a-ZIP to extract secret.txt, then use https://330k.github.io/misc_tools/unicode_steganography.html to decode zero-width characters hidden in message.  
