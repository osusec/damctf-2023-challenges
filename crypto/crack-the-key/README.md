# crack-the-key

## Description

```
I have used an RSA key to encrypt the flag. Can you find a way to reconstruct the private
key using the public key? (Check out factordb.com to help with brute forcing)
```

### Flag

`dam{4lw4y5_u53_l4r63_r54_k3y5}`

## Checklist for Author

Mostly N/A

* [x] There is at least one test exploit/script in the `tests` directory

## Info

Use factordb to get primes, then decrypt

